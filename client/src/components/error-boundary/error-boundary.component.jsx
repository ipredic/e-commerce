import React from "react";
import {
  ErrorImageContainer,
  ErrorImageText,
  ErrorImageOverlay,
} from "./error-boundary.styles";

export default class ErrorBoundary extends React.Component {
  state = {
    hasErrored: false,
  };

  static getDerivedStateFromError(error) {
    return { hasErrored: true };
  }

  componentDidCatch(error, info) {
    console.log("Error:", error, "Info", info);
  }

  render() {
    if (this.state.hasErrored) {
      return (
        <ErrorImageOverlay>
          <ErrorImageContainer imageUrl={"https://i.imgur.com/yW2W9SC.png"} />
          <ErrorImageText>
            Sorry something went wrong, Please try again
          </ErrorImageText>
        </ErrorImageOverlay>
      );
    }

    return this.props.children;
  }
}
